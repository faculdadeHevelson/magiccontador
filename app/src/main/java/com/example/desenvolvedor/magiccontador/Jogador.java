package com.example.desenvolvedor.magiccontador;

/**
 * Created by Administrador on 26/06/2016.
 */
public class Jogador {

    private Integer id;
    private String nome;
    private Integer vitorias;
    private Integer derrotas;

    public Jogador(Integer id, String nome, Integer vitorias, Integer derrotas) {
        this.id = id;
        this.nome = nome;
        this.vitorias = vitorias;
        this.derrotas = derrotas;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public Integer getVitorias() {
        return vitorias;
    }

    public Integer getDerrotas() {
        return derrotas;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setVitorias(Integer vitorias) {
        this.vitorias = vitorias;
    }

    public void setDerrotas(Integer derrotas) {
        this.derrotas = derrotas;
    }
}
