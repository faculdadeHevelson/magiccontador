package com.example.desenvolvedor.magiccontador;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private JogadorDialog cadastroJogador;

    private Button mais1P1,menos1P1,mais5P1,menos5P1;
    private Button mais1P2,menos1P2,mais5P2,menos5P2;

    private TextView vidaP1, vidaP2, textViewP1Historico, textViewP2Historico, nomeJogador1, nomeJogador2;

    public ListView lista;

    Integer vidaAtual, vidaModificada, idPlayer1, idPlayer2;

    private Jogador jogador1 = new Jogador(0,"Jogador 1", 0, 0);
    private Jogador jogador2 = new Jogador(0,"Jogador 1", 0, 0);

    private String dadoJogado = "";

    List<String> nomesJogadores;
    List<String> idsJogadores;

    LayoutInflater li;
    LinearLayout listaDeJogadores;
    Dialog alert;
    BancoController crud = new BancoController(this);;
    private Spinner listaPlayers;
    private Button btnOk, btnCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cadastroJogador = new JogadorDialog(this);

        idPlayer1 = 0;
        idPlayer2 = 0;

        vidaP1 = (TextView) findViewById(R.id.txtVidaPlayer1);
        vidaP2 = (TextView) findViewById(R.id.txtVidaPlayer2);

        nomeJogador1 = (TextView) findViewById(R.id.txtNomePlayer1);
        nomeJogador2 = (TextView) findViewById(R.id.txtNomePlayer2);

        textViewP1Historico = (TextView) findViewById(R.id.textHistoricoPlayer1);
        textViewP2Historico = (TextView) findViewById(R.id.textHistoricoPlayer2);

        mais1P1 = (Button) findViewById(R.id.btnMais1P1);
        mais5P1 = (Button) findViewById(R.id.btnMais5P1);
        menos1P1 = (Button) findViewById(R.id.btnMenos1P1);
        menos5P1 = (Button) findViewById(R.id.btnMenos5P1);

        mais1P2 = (Button) findViewById(R.id.btnMais1P2);
        mais5P2 = (Button) findViewById(R.id.btnMais5P2);
        menos1P2 = (Button) findViewById(R.id.btnMenos1P2);
        menos5P2 = (Button) findViewById(R.id.btnMenos5P2);

        mais1P1.setOnClickListener(this);
        mais5P1.setOnClickListener(this);
        menos1P1.setOnClickListener(this);
        menos5P1.setOnClickListener(this);

        mais1P2.setOnClickListener(this);
        mais5P2.setOnClickListener(this);
        menos1P2.setOnClickListener(this);
        menos5P2.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.opcoes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        if ( id == R.id.roll_dice ) {

            this.dialogDice();

        } else if ( id == R.id.new_player ) {

            cadastroJogador.show();

        } else if ( id == R.id.graficos_partidas ) {

            exibeEstatisticas();

        } else if ( id == R.id.novo_jogo ) {

            resetaJogo();
        }

        return true;
    }

    @Override
    public void onClick(View v) {

        if( v.getId() == R.id.btnMais1P1 ) {

            String text = trocaVida(vidaP1, 1, "+");

            listaHistorico(textViewP1Historico, text);
            System.out.println(jogador1.getId());
        } else if( v.getId() == R.id.btnMais5P1 ) {

            String text = trocaVida(vidaP1, 5, "+");

            listaHistorico(textViewP1Historico, text);
        } else if( v.getId() == R.id.btnMenos1P1 ) {

            String text = trocaVida(vidaP1, 1, "-");

            listaHistorico(textViewP1Historico, text);
        } else if( v.getId() == R.id.btnMenos5P1 ) {

            String text = trocaVida(vidaP1, 5, "-");

            listaHistorico(textViewP1Historico, text);
        } else if( v.getId() == R.id.btnMais1P2 ) {

            String text = trocaVida(vidaP2, 1, "+");

            listaHistorico(textViewP2Historico, text);
        } else if( v.getId() == R.id.btnMais5P2 ) {

            String text = trocaVida(vidaP2, 5, "+");

            listaHistorico(textViewP2Historico, text);
        } else if( v.getId() == R.id.btnMenos1P2 ) {

            String text = trocaVida(vidaP2, 1, "-");

            listaHistorico(textViewP2Historico, text);
        } else if( v.getId() == R.id.btnMenos5P2 ) {

            String text = trocaVida(vidaP2, 5, "-");

            listaHistorico(textViewP2Historico, text);
        } else if ( v.getId() == R.id.txtNomePlayer1 ) {

            showJogadores(1);
        } else if ( v.getId() == R.id.txtNomePlayer2 ) {

            showJogadores(2);
        }
    }

    private void dialogDice() {

        ArrayList<String> spinnerArray = new ArrayList<String>();
        spinnerArray.add("Selecione o dado");
        spinnerArray.add("d4");
        spinnerArray.add("d6");
        spinnerArray.add("d8");
        spinnerArray.add("d10");
        spinnerArray.add("d12");
        spinnerArray.add("d20");

        final Spinner spinner = new Spinner(this);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        spinner.setAdapter(spinnerArrayAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Escolha o dado");

        builder.setView(spinner);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dadoJogado = spinner.getSelectedItem().toString();

                mostraDado();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void mostraDado() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Resultado");

        Random random = new Random();
        int min = 1;
        int max;

        if ( dadoJogado == "d4" ) {

            max = 4;

        } else if ( dadoJogado == "d6" ) {

            max = 6;
        } else if ( dadoJogado == "d8" ) {

            max = 8;
        } else if ( dadoJogado == "d10" ) {

            max = 10;
        } else if ( dadoJogado == "d12" ) {

            max = 12;
        } else {

            max = 20;
        }

        Integer randomNum = random.nextInt((max - min) + 1) + min;

        TextView textDado = new TextView(this);
        textDado.setText(randomNum.toString());
        builder.setView(textDado);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();

            }
        });

        builder.show();
    }

    private String trocaVida( TextView vida, Integer valor, String operacao) {

        vidaAtual = Integer.parseInt(vida.getText().toString());

        if( operacao == "-") {
            vidaModificada = vidaAtual - valor;
        } else {
            vidaModificada = vidaAtual + valor;
        }

        String novoHistorico = vidaModificada + " (" + vidaAtual + " " + operacao + " " + valor + ")";

        vida.setText(vidaModificada.toString());

        return novoHistorico;
    }

    private void listaHistorico( TextView vidaPlayer, String novoHistorico ) {

        String historico = vidaPlayer.getText().toString();

        if ( historico != "" ) {
            historico += " \n";
        }

        historico += novoHistorico;

        vidaPlayer.setText(historico);

        if(vidaModificada <= 0 ) {
            verificaVencedor();
        }
    }

    public void showJogadores(final Integer jogadorNumero) {

        li = LayoutInflater.from(this);
        listaDeJogadores = (LinearLayout)li.inflate(R.layout.lista_jogadores, null);

        btnOk = (Button) listaDeJogadores.findViewById(R.id.btnJogadorSelecionado);
        btnCancel = (Button) listaDeJogadores.findViewById(R.id.btnCancelarSelecao);

        Cursor cursor = crud.carregaDados();

        idsJogadores = new ArrayList<String>();
        nomesJogadores = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                idsJogadores.add(cursor.getString(0));
                nomesJogadores.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        listaPlayers = (Spinner) listaDeJogadores.findViewById(R.id.spinnerJogadores);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, nomesJogadores);
        listaPlayers.setAdapter(spinnerArrayAdapter);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if( jogadorNumero == 1 ) {

                    Integer position = listaPlayers.getSelectedItemPosition();

                    nomeJogador1.setText(nomesJogadores.get(position));
                    jogador1.setId(Integer.parseInt(idsJogadores.get(position)));

                    alert.dismiss();
                } else {

                    Integer position = listaPlayers.getSelectedItemPosition();

                    nomeJogador2.setText(nomesJogadores.get(position));
                    jogador2.setId(Integer.parseInt(idsJogadores.get(position)));

                    alert.dismiss();
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert = new Dialog(this);
        alert.setTitle("Selecionar Jogador");
        alert.setContentView(listaDeJogadores);

        alert.show();
    }

    public void verificaVencedor() {

        if ( Integer.parseInt(vidaP2.getText().toString()) < Integer.parseInt(vidaP1.getText().toString()) ) {

            salvaVitoriaDerrota(jogador1, jogador2);

        } else {

            salvaVitoriaDerrota(jogador2, jogador1);

        }
    }

    public void salvaVitoriaDerrota(Jogador vendedor, Jogador perdedor) {

        if(vendedor.getId() != 0 ) {

            crud.salvaVitoria(vendedor.getId());
            crud.salvaDerrota(perdedor.getId());
        }

        resetaJogo();

        Toast.makeText(this, "Vitoria do jogador " + vendedor.getNome(), Toast.LENGTH_LONG).show();
    }

    public void resetaJogo() {

        vidaP1.setText("20");
        vidaP2.setText("20");
        textViewP1Historico.setText("");
        textViewP2Historico.setText("");

    }

    public void exibeEstatisticas() {

        Intent i = new Intent(this, Estatisticas.class);
        startActivity(i);
    }
}
