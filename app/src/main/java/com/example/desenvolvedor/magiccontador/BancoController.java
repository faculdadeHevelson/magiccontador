package com.example.desenvolvedor.magiccontador;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Administrador on 25/06/2016.
 */
public class BancoController {

    private SQLiteDatabase db;
    private CriaBanco banco;

    public BancoController(Context context){
        banco = new CriaBanco(context);
    }

    public String insereDado(String titulo, Integer autor, Integer editora){
        ContentValues valores;
        long resultado;

        db = banco.getWritableDatabase();
        valores = new ContentValues();
        valores.put(CriaBanco.NOME, titulo);
        valores.put(CriaBanco.VITORIAS, autor);
        valores.put(CriaBanco.DERROTAS, editora);

        resultado = db.insert(CriaBanco.TABELA, null, valores);
        db.close();

        if ( resultado == -1 ) {
            return "Erro ao cadastrar jogador";
        } else {
            return "Jogador salvo";
        }

    }

    public Cursor carregaDados(){
        Cursor cursor;
        String[] campos =  {banco.ID, banco.NOME};
        db = banco.getReadableDatabase();
        cursor = db.query(banco.TABELA, campos, null, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public Cursor carregaDadosListagem(){

        Cursor cursor;
        String[] campos =  {banco.ID, banco.NOME, banco.VITORIAS, banco.DERROTAS};

        db = banco.getReadableDatabase();
        cursor = db.query(banco.TABELA, campos, null, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();

        return cursor;
    }

    public Cursor carregaDadoById(int id){
        Cursor cursor;
        String[] campos =  {banco.ID, banco.NOME, banco.VITORIAS, banco.DERROTAS};
        String where = CriaBanco.ID + "=" + id;
        db = banco.getReadableDatabase();
        cursor = db.query(CriaBanco.TABELA,campos,where, null, null, null, null, null);

        if(cursor!=null){
            cursor.moveToFirst();
        }
        db.close();
        return cursor;
    }

    public void alteraRegistro(int id, String nome, Integer vitorias, Integer derrotas){
        ContentValues valores;
        String where;

        db = banco.getWritableDatabase();

        where = CriaBanco.ID + "=" + id;

        valores = new ContentValues();
        valores.put(CriaBanco.NOME, nome);
        valores.put(CriaBanco.VITORIAS, vitorias);
        valores.put(CriaBanco.DERROTAS, derrotas);

        db.update(CriaBanco.TABELA,valores,where,null);
        db.close();
    }

    public void salvaVitoria(Integer id) {

        Cursor cursor = carregaDadoById(id);

        ContentValues valores;
        String where;

        db = banco.getWritableDatabase();

        where = CriaBanco.ID + "=" + id;

        int vitorias = cursor.getInt(cursor.getColumnIndexOrThrow(CriaBanco.VITORIAS)) + 1;

        valores = new ContentValues();
        valores.put(CriaBanco.NOME, cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.NOME)));
        valores.put(CriaBanco.VITORIAS, vitorias);
        valores.put(CriaBanco.DERROTAS, cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.DERROTAS)));

        db.update(CriaBanco.TABELA,valores,where,null);
        db.close();
    }

    public void salvaDerrota(Integer id) {

        Cursor cursor = carregaDadoById(id);

        ContentValues valores;
        String where;

        db = banco.getWritableDatabase();

        where = CriaBanco.ID + "=" + id;

        int derrotas = cursor.getInt(cursor.getColumnIndexOrThrow(CriaBanco.DERROTAS)) + 1;

        valores = new ContentValues();
        valores.put(CriaBanco.NOME, cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.NOME)));
        valores.put(CriaBanco.VITORIAS, cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.VITORIAS)));
        valores.put(CriaBanco.DERROTAS, derrotas);

        db.update(CriaBanco.TABELA,valores,where,null);
        db.close();
    }

}
