package com.example.desenvolvedor.magiccontador;

import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrador on 25/06/2016.
 */
public class SelecionaJogadorDialog extends AppCompatActivity {

    LayoutInflater li;
    LinearLayout listaDeJogadores;
    Dialog alert;
    BancoController crud;
    final Context context;
    private Spinner listaPlayers;
    private Button btnOk, btnCancel;
    public Jogador player;


    SelecionaJogadorDialog(Context context) {

        this.context = context;
        crud = new BancoController(this.context);

        li = LayoutInflater.from(this.context);
        listaDeJogadores = (LinearLayout)li.inflate(R.layout.lista_jogadores, null);

        btnOk = (Button) listaDeJogadores.findViewById(R.id.btnJogadorSelecionado);
        btnCancel = (Button) listaDeJogadores.findViewById(R.id.btnCancelarSelecao);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });
    }

    public void showJogadores(Jogador player) {

        this.player = player;

        Cursor cursor = crud.carregaDados();

        List<String> idsJogadores = new ArrayList<String>();
        List<String> nomesJogadores = new ArrayList<String>();

        if (cursor.moveToFirst()) {
            do {
                idsJogadores.add(cursor.getString(0));
                nomesJogadores.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        System.out.println(idsJogadores);
//
//        String[] nomeCampos = new String[] {CriaBanco.ID, CriaBanco.NOME};
//        int[] idViews = new int[] {R.id.idJogador, R.id.nomeJogador};
//
        listaPlayers = (Spinner) listaDeJogadores.findViewById(R.id.spinnerJogadores);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this.context, android.R.layout.simple_spinner_dropdown_item, nomesJogadores);
        listaPlayers.setAdapter(spinnerArrayAdapter);
//
//        System.out.println("adapter foi");
        alert = new Dialog(this.context);
        alert.setTitle("Selecionar Jogador");
        alert.setContentView(listaDeJogadores);

        alert.show();

    }

    private void carregaJogadorSelecionado() {

    }
}
