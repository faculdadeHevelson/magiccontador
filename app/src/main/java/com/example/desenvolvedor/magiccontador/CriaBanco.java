package com.example.desenvolvedor.magiccontador;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrador on 25/06/2016.
 */
public class CriaBanco extends SQLiteOpenHelper {
    static final String NOME_BANCO = "jogadoresdatabase.db";
    static final String TABELA = "jogadores";
    static final String ID = "_id";
    static final String NOME = "nome";
    static final String VITORIAS = "vitorias";
    static final String DERROTAS = "derrotas";
    static final int VERSAO = 1;

    public CriaBanco(Context context){
        super(context, NOME_BANCO,null,VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+TABELA+" ( "
                + ID + " integer primary key autoincrement, "
                + NOME + " text, "
                + VITORIAS + " integer, "
                + DERROTAS + " integer "
                +" )";
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS" + TABELA);
        onCreate(db);
    }
}