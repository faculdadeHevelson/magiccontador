package com.example.desenvolvedor.magiccontador;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

public class Estatisticas extends AppCompatActivity {

    private ListView lista;

    LayoutInflater li;
    LinearLayout novoJogador;
    Dialog alert;
    Button cadastrar, cancelar;
    EditText namePlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statisticas);

        BancoController crud = new BancoController(getBaseContext());
        final Cursor cursor = crud.carregaDadosListagem();

        String[] nomeCampos = new String[] {CriaBanco.ID, CriaBanco.NOME, CriaBanco.VITORIAS, CriaBanco.DERROTAS};
        int[] idViews = new int[] {R.id.idJogador, R.id.nomeJogador, R.id.vitoriasJogador, R.id.derrotasJogador};

        SimpleCursorAdapter adaptador = new SimpleCursorAdapter(getBaseContext(), R.layout.custom_list_itens, cursor, nomeCampos, idViews, 0);
        lista = (ListView) findViewById(R.id.listViewEstatisticas);
        lista.setAdapter(adaptador);

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String codigo;
                cursor.moveToPosition(position);
                codigo = cursor.getString(cursor.getColumnIndexOrThrow(CriaBanco.ID));


            }
        });
    }
}
