package com.example.desenvolvedor.magiccontador;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by Administrador on 25/06/2016.
 */
public class JogadorDialog extends AppCompatActivity {

    LayoutInflater li;
    LinearLayout novoJogador;
    Dialog alert;
    Button cadastrar, cancelar;
    EditText namePlayer;

    JogadorDialog(final Context context) {

        li = LayoutInflater.from(context);
        novoJogador = (LinearLayout)li.inflate(R.layout.cadastro_jogador, null);

        namePlayer = (EditText) novoJogador.findViewById(R.id.nomeJogador);
        cancelar = (Button) novoJogador.findViewById(R.id.btnCancelJogador);
        cadastrar = (Button) novoJogador.findViewById(R.id.btnSalvarJogador);

        alert = new Dialog(context);
        alert.setTitle("Cadastrar Jogador");
        alert.setContentView(novoJogador);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BancoController crud = new BancoController(context);

                String nome = namePlayer.getText().toString();
                Integer vitorias = 0;
                Integer derrotas = 0;

                String resultado;

                resultado = crud.insereDado(nome, vitorias, derrotas);

                Toast.makeText(context, resultado, Toast.LENGTH_LONG).show();

            }
        });

    }

    public void show() {

        alert.show();
    }
}
